class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title
      t.string :director
      t.text :actors, array: true, default: []
      t.string :rating

      t.timestamps null: false
    end
  end
end
