class MovieSerializer < ActiveModel::Serializer
  attributes :id, :title, :director, :rating
end
