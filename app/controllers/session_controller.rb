class SessionController < ApplicationController
  def login
    user = User.find_by(email: params[:email]).try(:authenticate, params[:password])

    if user
      render json: user
    else
      render json: {error: "Invalid username or password"}, status: 422
    end
  end

  def register
    if user = User.find_by(email: params[:email])
      render json: {error: "Email already taken"}, status: 422
      return
    end

    user = User.new(email: params[:email], password: params[:password],
                    password_confirmation: params[:password_confirmation])
    if user.save
      render json: user
    else
      render json: {error: "Passwords don't match"}, status: 422
    end
  end
end
