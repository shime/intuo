class MoviesController < ApplicationController
  def index
    render json: Movie.search(params[:query], params[:order])
  end
end
