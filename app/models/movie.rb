class Movie < ActiveRecord::Base
  validates_format_of :title, with: /\S/
  validates_uniqueness_of :title

  scope :leaderboard, -> { order("rating desc") }

  def self.search(query = nil, order)
    order = order || "desc"

    if query
      where("'#{query}' = ANY (actors) OR " +
            "title ILIKE '%#{query}%' OR " +
            "director ILIKE '%#{query}%'").
      order("rating #{order}")
    else
      order("rating #{order}")
    end
  end
end
