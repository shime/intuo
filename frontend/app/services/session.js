import Ember from 'ember';

export default Ember.Service.extend({
  isLoggedIn: false,

  init(){
    if (this.readCookie('user')){
      this.setProperties({
        isLoggedIn: true,
        currentUser: this.readCookie('user')
      })
    }
  },

  logOut(){
    this.eraseCookie('user')
    this.set('isLoggedIn', false)
  },

  logIn(credentials){
    var self = this
    Ember.$.post("/login", credentials).then(function(response){
      self.createCookie('user', response.email)
      self.setProperties({
        isLoggedIn: true,
        currentUser: response.email,
        errors: undefined,
      })
    }, function(response){
      self.set("errors", response.responseJSON.error)
    })
  },

  register(credentials){
    var self = this
    Ember.$.post("/register", credentials).then(function(response){
      self.createCookie('user', response.email)
      self.setProperties({
        isLoggedIn: true,
        currentUser: response.email,
        errors: undefined,
      })
    }, function(response){
      self.set("errors", response.responseJSON.error)
    })
  },

  createCookie(name,value,days) {
    if (days) {
      var date = new Date();
      date.setTime(date.getTime()+(days*24*60*60*1000));
      var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
  },

  readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  },

  eraseCookie(name) {
    this.createCookie(name,"",-1);
  }
});
