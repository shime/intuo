import Ember from 'ember';

export default Ember.Component.extend({
  query: "",
  isAsc: Ember.computed.equal("order", "asc"),

  actions:{
    submit(order){
      this.sendAction('action', this.get('query'), order)
    },
  }
});
