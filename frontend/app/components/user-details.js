import Ember from 'ember';

export default Ember.Component.extend({
  email: '',
  password: '',
  passwordConfirmation: '',
  isRegister: false,
  session: Ember.inject.service(),

  actions: {
    logIn(){
      this.get('session').logIn(this.getProperties('email', 'password'))
    },
    register(){
      this.get('session').register({
        password_confirmation: this.get('passwordConfirmation'),
        password: this.get('password'),
        email: this.get('email')
      })
    },
  }
});
