import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('login')
  this.route('register')
  this.route('add-movie')
  this.route('edit-movie')
  this.route('leaderboard')
});

export default Router;
