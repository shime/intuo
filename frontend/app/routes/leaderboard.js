import Ember from 'ember';

export default Ember.Route.extend({
  queryParams: {
    query: {
      refreshModel: true
    },
    order: {
      refreshModel: true
    }
  },
  model(params){
    this.set('params', params)
    return Ember.$.get('movies', params)
  },

  setupController(controller, model){
    controller.setProperties({
      model: model.movies,
      query: this.get('params.query'),
      order: this.get('params.order') || "desc",
    })
  },

  actions: {
    performSearch(query, order){
      this.transitionTo('leaderboard', {queryParams: {query: query, order: order || "desc"}})
    }
  }

});
