task :scrape => :environment do
  Imdb::Top250.new.movies.each do |movie|
    Movie.create(title: movie.title.split("\n").try(:last).try(:strip),
                 director: movie.director.first,
                 actors: movie.cast_members,
                 rating: movie.rating)
  end
end
